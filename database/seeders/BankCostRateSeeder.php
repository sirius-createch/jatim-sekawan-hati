<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\InsuranceType;
use App\Models\BankCostRate;
use Illuminate\Database\Seeder;
use DB;

class BankCostRateSeeder extends Seeder
{
    public function run()
    {
        $params = [];
        foreach (Bank::all() as $bank) {
            foreach (InsuranceType::all() as $insuranteType) {
                $params[] = [
                    'min_value' => mt_rand(10000,20000),
                    'rate_value' => mt_rand(0,1),
                    'polish_cost' => mt_rand(10000,20000),
                    'stamp_cost' => mt_rand(10000,20000),
                    'desc' => "Seeder Bank $bank->name insurance type $insuranteType->name",
                    'bank_id' => $bank->id,
                    'insurance_type_id' => $insuranteType->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }
        }
        BankCostRate::insert($params);
    }
}
