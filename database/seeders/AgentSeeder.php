<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\Agent::factory(10)->create();
        \App\Models\Agent::all()->each(fn($agent) => $agent->branches()->sync($agent->branch_id));
    }
}
