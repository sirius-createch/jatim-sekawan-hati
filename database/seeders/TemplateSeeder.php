<?php

namespace Database\Seeders;

use App\Models\Template;
use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    public function run()
    {
        Template::create([
            'title' => "Kwitansi",
            'text' => '
<div style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: .8rem; line-height: 1.2rem;">
    <h1 style="text-align: center;">RECEIPT</h1>
    <div style="text-align: right;">
        <b><u>SERVICE CHARGE</u></b><br>
        NO. KWT: [[NoKwitansi]]
    </div>
    <div>
        Lampiran Ikhtisar Service Charge: [[JenisJaminan]] No. [[NoBond]]
    </div>
    <hr style="margin-block: 10px;">
    <table border="0" style="width: 100%;">
        <tr>
            <td style="width: 20%; vertical-align: top;"><u>Sudah diterima dari</u></td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[NamaPrincipal]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;"></td>
            <td style="width: 1%; vertical-align: top;"></td>
            <td style="width: 79%;">[[AlamatPrincipal]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;"><u>Jumlah uang sebesar</u></td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[PremiBayarTerbilang]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">Jumlah jaminan</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">1 (SATU) [[JenisJaminan]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">Asuransi</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[NamaAsuransi]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">Nilai Bond</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[NilaiJaminan]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">Pekerjaan</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[NamaProyek]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">Periode</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 79%;">[[JangkaAwal]] s.d. [[JangkaAkhir]] ([[JumlahHari]] HARI KALENDER)</td>
        </tr>
    </table>
    <br>
    <b>PERHITUNGAN</b>
    <table border="0" style="width: 100%;">
        <tr>
            <td style="width: 20%; vertical-align: top;">- Service Charge</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 49%;">[[ServiceCharge]]</td>
            <td style="width: 30%; text-align: right; vertical-align: top;" rowspan="3">SURABAYA, [[TanggalHariIni]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;">- Polis + Materai</td>
            <td style="width: 1%; vertical-align: top;">:</td>
            <td style="width: 49%;">[[BiayaAdmin]]</td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top;"></td>
            <td style="width: 1%; vertical-align: top;"></td>
            <td style="width: 49%;">[[PremiBayar]]</td>
        </tr>
    </table>
    <br>
    <div>[[NamaAgen]]</div>
    <br>
    <div style="text-align: center;">
        <small><b>Biaya mohon untuk ditransfer ke BANK SYARIAH INDONESIA, Rek 7184029833 a.n PT. JATIM SEKAWAN HATI</b></small>
    </div>
</div>
            ',
        ]);

        Template::create([
            'title' => "Surat Permohonan",
            'text' => '',
        ]);
    }
}
