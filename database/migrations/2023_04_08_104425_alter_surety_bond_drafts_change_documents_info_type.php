<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('surety_bond_drafts', function (Blueprint $table) {
            $table->text('document_number')->change();
            $table->text('document_title')->change();
        });
    }
};
