<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('principals', function (Blueprint $table) {
            $table->string('note')->nullable()->after('city_id');
        });
    }
    public function down()
    {
        Schema::table('principals', function (Blueprint $table) {
            $table->dropColumn('note');
        });
    }
};
