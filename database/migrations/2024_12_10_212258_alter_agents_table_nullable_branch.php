<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->unsignedBigInteger('branch_id')->nullable()->change();
        });
    }
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id')->change();
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }
};
