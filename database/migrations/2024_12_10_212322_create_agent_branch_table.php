<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('agent_branch', function (Blueprint $table) {
            $table->foreignId('agent_id')->constrained();
            $table->foreignId('branch_id')->constrained();
            $table->primary(['agent_id', 'branch_id']);
            $table->index(['agent_id', 'branch_id']);
        });
    }
    public function down()
    {
        Schema::dropIfExists('agent_branch');
    }
};
