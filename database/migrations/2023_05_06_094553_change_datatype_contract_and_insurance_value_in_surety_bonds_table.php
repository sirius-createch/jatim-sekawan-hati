<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('surety_bonds', function (Blueprint $table) {
            $table->decimal('contract_value', 20, 5)->change();
            $table->decimal('insurance_value', 20, 5)->change();
        });
    }
};
