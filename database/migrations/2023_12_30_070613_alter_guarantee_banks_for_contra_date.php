<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('guarantee_banks', function (Blueprint $table) {
            $table->date('contra_date')->nullable();
            $table->string('bond_number')->comment('contra_number')->change();
        });
    }
    public function down()
    {
        Schema::table('guarantee_banks', function (Blueprint $table) {
            $table->dropColumn('contra_date');
        });
    }
};
