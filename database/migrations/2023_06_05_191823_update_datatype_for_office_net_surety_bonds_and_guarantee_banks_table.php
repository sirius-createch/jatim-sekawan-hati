<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('surety_bonds', function (Blueprint $table) {
            $table->decimal('office_net', 20, 5)->change();
            $table->decimal('office_net_total', 20, 5)->change();
        });

        Schema::table('surety_bond_drafts', function (Blueprint $table) {
            $table->decimal('office_net', 20, 5)->change();
            $table->decimal('office_net_total', 20, 5)->change();
        });

        Schema::table('guarantee_banks', function (Blueprint $table) {
            $table->decimal('office_net', 20, 5)->change();
            $table->decimal('office_net_total', 20, 5)->change();
        });

        Schema::table('guarantee_bank_drafts', function (Blueprint $table) {
            $table->decimal('office_net', 20, 5)->change();
            $table->decimal('office_net_total', 20, 5)->change();
        });
    }
};
