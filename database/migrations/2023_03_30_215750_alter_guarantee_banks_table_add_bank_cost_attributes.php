<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('guarantee_banks', function (Blueprint $table) {
            $table->unsignedInteger('bank_cost_polish_cost');
            $table->unsignedInteger('bank_cost_stamp_cost');
            $table->unsignedDecimal('bank_cost_rate', 8, 3);
            $table->unsignedDecimal('bank_cost_net',10,2);
            $table->unsignedDecimal('bank_cost_net_total',10,2);
        });
    }
    public function down()
    {
        Schema::table('guarantee_banks', function (Blueprint $table) {
            $table->dropColumn('bank_cost_polish_cost');
            $table->dropColumn('bank_cost_stamp_cost');
            $table->dropColumn('bank_cost_rate');
            $table->dropColumn('bank_cost_net');
            $table->dropColumn('bank_cost_net_total');
        });
    }
};
