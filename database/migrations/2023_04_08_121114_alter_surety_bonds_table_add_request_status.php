<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('surety_bonds', function (Blueprint $table) {
            $table->string('request_status')->default('Belum Diajukan');
        });
    }
    public function down()
    {
        Schema::table('surety_bonds', function (Blueprint $table) {
            $table->dropColumn('request_status');
        });
    }
};
