@php
    $status = $model->fs_name;
@endphp
<x-badge rounded face="label-{{ \App\Models\GuaranteeBank::mappingFinanceStatusColors($status) }}" icon="{{ \App\Models\GuaranteeBank::mappingFinanceStatusIcons($status) }}">{{ \App\Models\GuaranteeBank::mappingFinanceStatusNames($status) }}</x-badge>
