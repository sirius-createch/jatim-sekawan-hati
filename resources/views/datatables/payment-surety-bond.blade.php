@php
    $status = $model->fs_name;
@endphp
<x-badge rounded face="label-{{ \App\Models\SuretyBond::mappingFinanceStatusColors($status) }}" icon="{{ \App\Models\SuretyBond::mappingFinanceStatusIcons($status) }}">{{ \App\Models\SuretyBond::mappingFinanceStatusNames($status) }}</x-badge>
