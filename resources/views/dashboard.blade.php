@extends('layouts.main', ['title' => 'Dasbor'])

@section('contents')
    {{-- Summary --}}
    <div class="row">
        <div class="col-md-4 mb-3">
            <x-card>
                <div class="d-flex display-5 align-items-center">
                    <div class="p-0 m-0 text-primary">
                        <i class="bx bxs-user-account"></i>
                    </div>
                    <div class="border-start ps-3 ms-3">
                        <span id="total-agent">{{ $agen }}</span><br>
                        <small class="h6">Total Agen</small>
                    </div>
                </div>
            </x-card>
        </div>

        <div class="col-md-4 mb-3">
            <x-card>
                <div class="d-flex display-5 align-items-center">
                    <div class="p-0 m-0 text-danger">
                        <i class="bx bx-receipt"></i>
                    </div>
                    <div class="border-start ps-3 ms-3">
                        <span id="total-surety-bond-not-paid">{{ $sb_non_lunas }}</span><br>
                        <small class="h6">Total Surety Bond</small>
                    </div>
                </div>
            </x-card>
        </div>
        <div class="col-md-4 mb-4">
            <x-card>
                <div class="d-flex display-5 align-items-center">
                    <div class="p-0 m-0 text-success">
                        <i class="bx bxs-receipt"></i>
                    </div>
                    <div class="border-start ps-3 ms-3">
                        <span id="total-surety-bond-not-paid">{{ $bg_non_lunas }}</span><br>
                        <small class="h6">Total Bank Garansi</small>
                    </div>
                </div>
            </x-card>
        </div>
    </div>

    <x-card class="mb-4">
        <div class="income-chart-container">
            <canvas id="chart_sb"></canvas>
        </div>
    </x-card>

    <x-card class="mb-4">
        <div class="expense-chart-container">
            <canvas id="chart_bg"></canvas>
        </div>
    </x-card>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const labels_sb = [
            @json($data_sbs[0]['bulan']),
            @json($data_sbs[1]['bulan']),
            @json($data_sbs[2]['bulan']),
            @json($data_sbs[3]['bulan']),
            @json($data_sbs[4]['bulan']),
            @json($data_sbs[5]['bulan']),
            @json($data_sbs[6]['bulan']),
            @json($data_sbs[7]['bulan']),
            @json($data_sbs[8]['bulan']),
            @json($data_sbs[9]['bulan']),
            @json($data_sbs[10]['bulan']),
            @json($data_sbs[11]['bulan']),
        ];
        const labels_bg = [
            @json($data_bgs[0]['bulan']),
            @json($data_bgs[1]['bulan']),
            @json($data_bgs[2]['bulan']),
            @json($data_bgs[3]['bulan']),
            @json($data_bgs[4]['bulan']),
            @json($data_bgs[5]['bulan']),
            @json($data_bgs[6]['bulan']),
            @json($data_bgs[7]['bulan']),
            @json($data_bgs[8]['bulan']),
            @json($data_bgs[9]['bulan']),
            @json($data_bgs[10]['bulan']),
            @json($data_bgs[11]['bulan']),
        ];
        const labels = [
            'JANUARI',
            'FEBRUARI',
            'MARET',
            'APRIL',
            'MEI',
            'JUNI',
            'JULI',
            'AGUSTUS',
            'SEPTEMBER',
            'OKTOBER',
            'NOVEMBER',
            'DESEMBER',
        ];
        // SB
        var canvas_sb = document.getElementById('chart_sb');
        var data_sb = {
            labels: labels_sb,
            datasets: [
                {
                    label: "Total Net",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [
                        @json($data_sbs[0]['total_profit']),
                        @json($data_sbs[1]['total_profit']),
                        @json($data_sbs[2]['total_profit']),
                        @json($data_sbs[3]['total_profit']),
                        @json($data_sbs[4]['total_profit']),
                        @json($data_sbs[5]['total_profit']),
                        @json($data_sbs[6]['total_profit']),
                        @json($data_sbs[7]['total_profit']),
                        @json($data_sbs[8]['total_profit']),
                        @json($data_sbs[9]['total_profit']),
                        @json($data_sbs[10]['total_profit']),
                        @json($data_sbs[11]['total_profit']),
                    ],
                }
            ]
        };
        var option_sb = {
            showLines: true,
            onClick: (e, activeEls) => {
                let datasetIndex = activeEls[0].datasetIndex;
                let dataIndex = activeEls[0].index;
                let datasetLabel = e.chart.data.datasets[datasetIndex].label;
                let value = e.chart.data.datasets[datasetIndex].data[dataIndex];
                let label = e.chart.data.labels[dataIndex];

                const tgl = label.split(" ")
                let bulan = tgl[0]
                let tahun = tgl[1]

                if(bulan == 'JANUARI'){bulan = '01'}
                else if(bulan == 'FEBRUARI'){bulan = '02'}
                else if(bulan == 'MARET'){bulan = '03'}
                else if(bulan == 'APRIL'){bulan = '04'}
                else if(bulan == 'MEI'){bulan = '05'}
                else if(bulan == 'JUNI'){bulan = '06'}
                else if(bulan == 'JULI'){bulan = '07'}
                else if(bulan == 'AGUSTUS'){bulan = '08'}
                else if(bulan == 'SEPTEMBER'){bulan = '09'}
                else if(bulan == 'OKTOBER'){bulan = '10'}
                else if(bulan == 'NOVEMBER'){bulan = '11'}
                else if(bulan == 'DESEMBER'){bulan = '12'}

                var start = tahun+'-'+bulan+'-01'
                var end = tahun+'-'+bulan+'-31'

                @if ($global->currently_on == 'main')
                    var url = "{{ route('main.sb-reports.production', ['startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif
                @if ($global->currently_on == 'regional')
                    var url = "{{ route('regional.sb-reports.production', ['regional' => $global->regional, 'startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif
                @if ($global->currently_on == 'branch')
                    var url = "{{ route('branch.sb-reports.production', ['regional' => $global->regional, 'branch' => $global->branch, 'startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif

                var url2 = url.replace('-start-',start)
                var url3 = url2.replace('-end-',end)
                window.location.href = url3.replace('&amp;','&')
            },
            plugins: {
                title: {
                    display: true,
                    text: 'Setor Kantor Surety Bond',
                    padding: {
                        top: 0,
                        bottom: 0
                    },
                    font: {
                        size: 24,
                        style: 'italic',
                        family: 'Helvetica Neue'
                    }
                }
            }
        };
        var sb = new Chart(canvas_sb,{
            type: 'line',
            data: data_sb,
            options: option_sb,
        });
        // ==================

        // BG
        var canvas_bg = document.getElementById('chart_bg');
        var data_bg = {
            labels: labels_bg,
            datasets: [
                {
                    label: "Total Net",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [
                        @json($data_bgs[0]['total_profit']),
                        @json($data_bgs[1]['total_profit']),
                        @json($data_bgs[2]['total_profit']),
                        @json($data_bgs[3]['total_profit']),
                        @json($data_bgs[4]['total_profit']),
                        @json($data_bgs[5]['total_profit']),
                        @json($data_bgs[6]['total_profit']),
                        @json($data_bgs[7]['total_profit']),
                        @json($data_bgs[8]['total_profit']),
                        @json($data_bgs[9]['total_profit']),
                        @json($data_bgs[10]['total_profit']),
                        @json($data_bgs[11]['total_profit']),
                    ],
                }
            ]
        };
        var option_bg = {
            showLines: true,
            onClick: (e, activeEls) => {
                let datasetIndex = activeEls[0].datasetIndex;
                let dataIndex = activeEls[0].index;
                let datasetLabel = e.chart.data.datasets[datasetIndex].label;
                let value = e.chart.data.datasets[datasetIndex].data[dataIndex];
                let label = e.chart.data.labels[dataIndex];


                const tgl = label.split(" ")
                let bulan = tgl[0]
                let tahun = tgl[1]

                if(bulan == 'JANUARI'){bulan = '01'}
                else if(bulan == 'FEBRUARI'){bulan = '02'}
                else if(bulan == 'MARET'){bulan = '03'}
                else if(bulan == 'APRIL'){bulan = '04'}
                else if(bulan == 'MEI'){bulan = '05'}
                else if(bulan == 'JUNI'){bulan = '06'}
                else if(bulan == 'JULI'){bulan = '07'}
                else if(bulan == 'AGUSTUS'){bulan = '08'}
                else if(bulan == 'SEPTEMBER'){bulan = '09'}
                else if(bulan == 'OKTOBER'){bulan = '10'}
                else if(bulan == 'NOVEMBER'){bulan = '11'}
                else if(bulan == 'DESEMBER'){bulan = '12'}

                var start = tahun+'-'+bulan+'-01'
                var end = tahun+'-'+bulan+'-31'

                @if ($global->currently_on == 'main')
                    var url = "{{ route('main.bg-reports.production', ['startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif
                @if ($global->currently_on == 'regional')
                    var url = "{{ route('regional.bg-reports.production', ['regional' => $global->regional ?? '', 'startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif
                @if ($global->currently_on == 'branch')
                    var url = "{{ route('branch.bg-reports.production', ['regional' => $global->regional ?? '', 'branch' => $global->branch ?? '', 'startDate' => '-start-', 'endDate' => '-end-']) }}"
                @endif

                var url2 = url.replace('-start-',start)
                var url3 = url2.replace('-end-',end)
                window.location.href = url3.replace('&amp;','&')
            },
            plugins: {
                title: {
                    display: true,
                    text: 'Setor Kantor Bank Garansi',
                    padding: {
                        top: 0,
                        bottom: 0
                    },
                    font: {
                        size: 24,
                        style: 'italic',
                        family: 'Helvetica Neue'
                    }
                }
            }
        };
        var bg = new Chart(canvas_bg,{
            type: 'line',
            data: data_bg,
            options: option_bg,
        });
        // ==================
    </script>
@endpush
