<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Exception;

class UserController extends Controller
{
    public function login(Request $request){
        try {
            if(!$request->has('username')) throw new Exception("Username wajib diisi", 422);
            if(!$request->has('password')) throw new Exception("Password wajib diisi", 422);

            if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){
                $user = Auth::user();
                $token = $user->createToken($user->username);
                $token->accessToken->update([
                    'expires_at' => date('Y-m-d H:i:s',strtotime('+5 minutes'))
                ]);
                $response = $this->response('OK',true,[
                    'token' => $token->plainTextToken,
                    'expires_at' => $token->accessToken->expires_at
                ]);
                $httpCode = 200;
            }else{
                throw new Exception("Akun tidak ditemukan, harap periksa kembali inputan Anda", 422);
            }
        } catch (Exception $e) {
            $httpCode = $this->httpErrorCode($e->getCode());
            $response = $this->errorResponse($e->getMessage());
        }
        return response()->json($response, $httpCode);
    }
}
