<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\PrincipalUpdateRequest;
use App\Models\Principal;
use Exception;
use DB;

class PrincipalController extends Controller
{
    public function update(PrincipalUpdateRequest $request){
        try {
            DB::beginTransaction();
            Principal::updateJamsyarStatus($request->validated());
            $http_code = 200;
            $response = $this->updateResponse();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $this->httpErrorCode($e->getCode());
            $response = $this->errorResponse($e->getMessage());
        }

        return response()->json($response, $http_code);
    }
}
