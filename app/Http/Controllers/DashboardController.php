<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Branch;
use App\Models\Payment;
use App\Models\Instalment;
use App\Models\SuretyBond;
use App\Models\SuretyBondStatus;
use Illuminate\Http\Request;
use App\Models\GuaranteeBankStatus;
use App\Models\GuaranteeBank;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class DashboardController extends Controller
{
    public function __invoke(?Branch $regional = null, ?Branch $branch = null)
    {
        $labels = [
            'JANUARI',
            'FEBRUARI',
            'MARET',
            'APRIL',
            'MEI',
            'JUNI',
            'JULI',
            'AGUSTUS',
            'SEPTEMBER',
            'OKTOBER',
            'NOVEMBER',
            'DESEMBER',
        ];

        $agen_count = Agent::all()->count();
        if($regional != null){
            // Cards Data
            $sb_not_paid = DB::table('surety_bonds')
                        ->join('surety_bond_statuses', 'surety_bonds.id', '=', 'surety_bond_statuses.surety_bond_id')
                        ->join('branches', 'surety_bonds.branch_id', '=', 'branches.id')
                        ->where('surety_bond_statuses.status_id','=',14)
                        ->where('branches.regional_id','=',$regional->id)
                        ->count();
            $bg_not_paid = DB::table('guarantee_banks')
                        ->join('guarantee_bank_statuses', 'guarantee_banks.id', '=', 'guarantee_bank_statuses.guarantee_bank_id')
                        ->join('branches', 'guarantee_banks.branch_id', '=', 'branches.id')
                        ->where('guarantee_bank_statuses.status_id','=',14)
                        ->where('branches.regional_id','=',$regional->id)
                        ->count();

            // Charts Data
            $data_sb = DB::table('surety_bonds as sb')
                ->select(DB::raw('SUM(sb.office_net_total) as total_profit, MONTH(sb.created_at) as month, YEAR(sb.created_at) as tahun'))
                ->join('branches as b', 'sb.branch_id', '=', 'b.id')
                ->groupBy(DB::raw('MONTH(sb.created_at), YEAR(sb.created_at)'))
                ->where('b.regional_id','=',$regional->id)
                ->limit(12)
                ->get();
            $data_bg = DB::table('guarantee_banks as gb')
                ->select(DB::raw('SUM(gb.office_net_total) as total_profit, MONTH(gb.created_at) as month, YEAR(gb.created_at) as tahun'))
                ->join('branches as b', 'gb.branch_id', '=', 'b.id')
                ->groupBy(DB::raw('MONTH(gb.created_at), YEAR(gb.created_at)'))
                ->where('b.regional_id','=',$regional->id)
                ->limit(12)
                ->get();
            if($branch != null){
                // Cards Data
                $sb_not_paid = DB::table('surety_bonds')
                        ->join('surety_bond_statuses', 'surety_bonds.id', '=', 'surety_bond_statuses.surety_bond_id')
                        ->where('surety_bond_statuses.status_id','=',14)
                        ->where('surety_bonds.branch_id','=',$branch->id)
                        ->count();
                $bg_not_paid = DB::table('guarantee_banks')
                        ->join('guarantee_bank_statuses', 'guarantee_banks.id', '=', 'guarantee_bank_statuses.guarantee_bank_id')
                        ->where('guarantee_bank_statuses.status_id','=',14)
                        ->where('guarantee_banks.branch_id','=',$branch->id)
                        ->count();

                // Charts Data
                $data_sb = DB::table('surety_bonds as sb')
                    ->select(DB::raw('SUM(sb.office_net_total) as total_profit, MONTH(sb.created_at) as month, YEAR(sb.created_at) as tahun'))
                    ->groupBy(DB::raw('MONTH(sb.created_at), YEAR(sb.created_at)'))
                    ->where('sb.branch_id','=',$branch->id)
                    ->limit(12)
                    ->get();
                $data_bg = DB::table('guarantee_banks as gb')
                    ->select(DB::raw('SUM(gb.office_net_total) as total_profit, MONTH(gb.created_at) as month, YEAR(gb.created_at) as tahun'))
                    ->groupBy(DB::raw('MONTH(gb.created_at), YEAR(gb.created_at)'))
                    ->where('gb.branch_id','=',$branch->id)
                    ->limit(12)
                    ->get();
            }
        }else{
            $bg_not_paid = GuaranteeBankStatus::where('status_id','=',14)->count();
            $sb_not_paid = SuretyBondStatus::where('status_id','=',14)->count();

            // Charts Data
            $data_sb = DB::table('surety_bonds as sb')
                ->select(DB::raw('SUM(sb.office_net_total) as total_profit, MONTH(sb.created_at) as month, YEAR(sb.created_at) as tahun'))
                ->groupBy(DB::raw('MONTH(sb.created_at), YEAR(sb.created_at)'))
                ->limit(12)
                ->get();
            $data_bg = DB::table('guarantee_banks as gb')
                ->select(DB::raw('SUM(gb.office_net_total) as total_profit, MONTH(gb.created_at) as month, YEAR(gb.created_at) as tahun'))
                ->groupBy(DB::raw('MONTH(gb.created_at), YEAR(gb.created_at)'))
                ->limit(12)
                ->get();
        }

        // Surety Bonds
        $data_sb_final = [];
        if($data_sb->isEmpty()){
            for($i = 0; $i < 12; $i++){
                $data_sb_final[$i]['total_profit'] = 0;
                $data_sb_final[$i]['month'] = '-';
                $data_sb_final[$i]['bulan'] = 'Tidak ada data';
            }
        }else{
            $idx = 0;
            for($j = $idx; $j < (12-(count($data_sb)+$data_sb[0]->month-1)); $j++){
                $data_sb_final[$j]['total_profit'] = 0;
                $data_sb_final[$j]['month'] = 13+$j-(12-(count($data_sb)+$data_sb[0]->month-1));
                $data_sb_final[$j]['bulan'] = $labels[12+$j-(12-(count($data_sb)+$data_sb[0]->month-1))].' '.$data_sb[0]->tahun-1;
                $idx++;
            }
            $count = 12-(count($data_sb)+$idx);
            for($j = 1; $j < $count+1; $j++){
                $data_sb_final[$idx]['total_profit'] = 0;
                $data_sb_final[$idx]['month'] = $j;
                $data_sb_final[$idx]['bulan'] = $labels[$j-1].' '.$data_sb[0]->tahun;
                $idx++;
            }
            for($i = 0; $i < count($data_sb); $i++){
                $data_sb_final[$idx]['total_profit'] = $data_sb[$i]->total_profit;
                $data_sb_final[$idx]['month'] = $data_sb[$i]->month;
                $data_sb_final[$idx]['bulan'] = $labels[$data_sb[$i]->month-1].' '.$data_sb[$i]->tahun;
                $idx++;
            }
        }

        // Bank Garansi
        $data_bg_final = [];
        if($data_bg->isEmpty()){
            for($i = 0; $i < 12; $i++){
                $data_bg_final[$i]['total_profit'] = 0;
                $data_bg_final[$i]['month'] = '-';
                $data_bg_final[$i]['bulan'] = 'Tidak ada data';
            }
        }else{
            $idx = 0;
            for($j = $idx; $j < (12-(count($data_bg)+$data_bg[0]->month-1)); $j++){
                $data_bg_final[$j]['total_profit'] = 0;
                $data_bg_final[$j]['month'] = 13+$j-(12-(count($data_bg)+$data_bg[0]->month-1));
                $data_bg_final[$j]['bulan'] = $labels[12+$j-(12-(count($data_bg)+$data_bg[0]->month-1))].' '.$data_bg[0]->tahun-1;
                $idx++;
            }
            $count = 12-(count($data_bg)+$idx);
            for($j = 1; $j < $count+1; $j++){
                $data_bg_final[$idx]['total_profit'] = 0;
                $data_bg_final[$idx]['month'] = $j;
                $data_bg_final[$idx]['bulan'] = $labels[$j-1].' '.$data_bg[0]->tahun;
                $idx++;
            }
            for($i = 0; $i < count($data_bg); $i++){
                $data_bg_final[$idx]['total_profit'] = $data_bg[$i]->total_profit;
                $data_bg_final[$idx]['month'] = $data_bg[$i]->month;
                $data_bg_final[$idx]['bulan'] = $labels[$data_bg[$i]->month-1].' '.$data_bg[$i]->tahun;
                $idx++;
            }
        }

        return view('dashboard', [
            'agen' => $agen_count,
            'bg_non_lunas' => $bg_not_paid,
            'sb_non_lunas' => $sb_not_paid,
            'data_sbs' => $data_sb_final,
            'data_bgs' => $data_bg_final,
        ]);
    }
}
