<?php

namespace App\Http\Controllers;

use App\Helpers\Sirius;
use App\Http\Requests\BankCostRateRequest;
use App\Models\BankCostRate;
use Illuminate\Http\Request;
use Exception;
use DB;

class BankCostRateController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = BankCostRate::with('bank','insurance_type')->select('bank_cost_rates.*')->whereNull('guarantee_bank_id');
            return datatables()->of($data)
            ->addIndexColumn()
            ->editColumn('min_value', fn($rate) => Sirius::toRupiah($rate->min_value))
            ->editColumn('rate_value', fn($rate) => str_replace('.', ',', $rate->rate_value))
            ->editColumn('polish_cost', fn($rate) => Sirius::toRupiah($rate->polish_cost))
            ->editColumn('stamp_cost', fn($rate) => Sirius::toRupiah($rate->stamp_cost))
            ->editColumn('action', 'datatables.actions-show-delete')
            ->toJson();
        }
        return view('master.bank-cost-rates');
    }

    public function create()
    {
    }

    public function store(BankCostRateRequest $request)
    {
        try {
            DB::beginTransaction();
            BankCostRate::buat($request->validated());
            $http_code = 200;
            $response = $this->storeResponse();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $this->httpErrorCode($e->getCode());
            $response = $this->errorResponse($e->getMessage());
        }

        return response()->json($response, $http_code);
    }

    public function show(BankCostRate $bankCostRate)
    {
        $bankCostRate->bank;
        $bankCostRate->insurance_type;
        return response()->json($this->showResponse($bankCostRate->toArray()));
    }

    public function edit(BankCostRate $bankCostRate)
    {
    }

    public function update(BankCostRateRequest $request, BankCostRate $bankCostRate)
    {
        try {
            DB::beginTransaction();
            $bankCostRate->ubah($request->validated());
            $http_code = 200;
            $response = $this->updateResponse();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $this->httpErrorCode($e->getCode());
            $response = $this->errorResponse($e->getMessage());
        }

        return response()->json($response, $http_code);
    }

    public function destroy(BankCostRate $bankCostRate)
    {
        try {
            DB::beginTransaction();
            $bankCostRate->hapus();
            $http_code = 200;
            $response = $this->destroyResponse();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $this->httpErrorCode($e->getCode());
            $response = $this->errorResponse($e->getMessage());
        }

        return response()->json($response, $http_code);
    }
}
