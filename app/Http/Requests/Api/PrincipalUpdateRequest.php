<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PrincipalUpdateRequest extends FormRequest
{
    /**
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'status' => 'required',
            'keterangan' => 'nullable',
            'kode_unik_principal' => 'required',
            'nama_principal' => 'required',
        ];
    }
}
