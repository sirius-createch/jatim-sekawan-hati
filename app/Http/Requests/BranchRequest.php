<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'is_regional' => 'required',
            'jamsyar_username' => 'required',
            'jamsyar_password' => 'required',
            'login_name' => 'required',
            'login_username' => 'required',
            'login_password' => 'required|confirmed',
            'regionalId' => 'required_if:is_regional,0'
        ];

        if ($this->getMethod() != 'POST') { // update
            $rules['login_password'] = 'nullable|confirmed';
        }

        return $rules;

    }
    public function attributes()
    {
        return [
            'name' => 'Nama',
            'jamsyar_username' => 'Username Jamsyar',
            'jamsyar_password' => 'Password Jamsyar',
            'login_name' => 'Nama Pengguna',
            'login_username' => 'Username Pengguna',
            'login_password' => 'Password Pengguna',
            'regionalId' => 'Regional'
        ];
    }
    public function messages()
    {
        return [
            'jamsyar_username.required_if' => ':attribute harus diisi',
            'jamsyar_password.required_if' => ':attribute harus diisi',
            'regionalId.required_if' => ':attribute harus diisi',
        ];
    }
}
