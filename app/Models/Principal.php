<?php

namespace App\Models;


use DB;
use Exception;
use App\Models\Scoring;
use App\Helpers\Sirius;
use App\Helpers\Jamsyar;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class Principal extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'domicile',
        'score',
        'npwp_number',
        'npwp_expired_at',
        'nib_number',
        'nib_expired_at',
        'pic_name',
        'pic_position',
        'jamsyar_id',
        'jamsyar_code',
        'status',
        'city_id',
        'note'
    ];

    protected $appends = ['npwp_expired_at_converted', 'nib_expired_at_converted'];

    // Accessors

    public function scoreColor(): Attribute
    {
        return Attribute::make(get: function () {
            return match (true) {
                $this->score >= 6.6 => 'success',
                $this->score >= 3.3 => 'warning',
                default => 'danger',
            };
        });
    }

    public function npwpExpiredAtConverted(): Attribute
    {
        return Attribute::make(get: fn() => $this->npwp_expired_at ? strtoupper(Sirius::toLongDate($this->npwp_expired_at)) : null);
    }

    public function nibExpiredAtConverted(): Attribute
    {
        return Attribute::make(get: fn() => $this->nib_expired_at ? strtoupper(Sirius::toLongDate($this->nib_expired_at)) : null);
    }

    // Relations

    public function city(){
        return $this->belongsTo(City::class);
    }
    public function certificates(){
        return $this->hasMany(Certificate::class);
    }
    public function certificate(){
        return $this->hasOne(Certificate::class)->latestOfMany();
    }
    public function files(): HasMany {
        return $this->hasMany(PrincipalFile::class, 'principal_id', 'id');
    }
    public function scorings(){
        return $this->belongsToMany(Scoring::class)->withTimestamps();
    }
    private static function fetch(object $args){
        $certificates = [];
        if(isset($args->certificate['number'])){
            for ($i=0; $i < count($args->certificate['number']); $i++) {
                $certificates[] = [
                    'number' => $args->certificate['number'][$i],
                    'expired_at' => $args->certificate['expiredAt'][$i],
                ];
            }
        }
        $files = [];
        if(isset($args->file['name'])){
            foreach ($args->file['name'] as $id => $fileName) {
                $files[] = [
                    'id' => $id,
                    'name' => $args->file['name'][$id] == '-' ? null : $args->file['name'][$id],
                    'upload' => null,
                    'is_deleted' => !empty($args->file['is_deleted'][$id]) ? true : false,
                ];
            }
        }
        if(isset($args->fileNew['name'])){
            foreach ($args->fileNew['name'] as $id => $fileName) {
                $files[] = [
                    'id' => null,
                    'name' => $args->fileNew['name'][$id],
                    'upload' => $args->fileNew['upload'][$id] ?? null,
                    'is_deleted' => false,
                ];
            }
        }
        $scorings = [];
        if(isset($args->scoring)){
            $scorings = array_values(collect($args->scoring)->map(function($item,$key){return $key;})->all());
        }
        return (object)[
            'principal' => [
                'name' => $args->info['name'],
                'email' => $args->info['email'],
                'phone' => $args->info['phone'],
                'domicile' => $args->info['domicile'],
                'address' => $args->info['address'],
                'pic_name' => $args->info['picName'],
                'pic_position' => $args->info['picPosition'],
                'npwp_number' => $args->info['npwpNumber'],
                'npwp_expired_at' => $args->info['npwpExpiredAt'],
                'nib_number' => $args->info['nibNumber'],
                'nib_expired_at' => $args->info['nibExpiredAt'],
                'city_id' => $args->info['cityId'],
                'province_id' => $args->info['provinceId'],
                'jamsyar_id' => $args->info['jamsyarId'],
                'jamsyar_code' => $args->info['jamsyarCode'],
                'status' => 'Belum Sinkron',
                'score' => 0
            ],
            'scoring' => $scorings,
            'certificate' => $certificates,
            'file' => $files,
        ];
    }
    public static function buat(array $params): self{
        $request = self::fetch((object)$params);
        $principal = self::create($request->principal);
        $principal->scorings()->sync($request->scoring);
        $principal->certificates()->createMany($request->certificate);
        $principal->upload($request->file);
        $principal->updateScore();
        return $principal;
    }
    public function ubah(array $params): bool{
        $request = self::fetch((object)$params);
        $this->update($request->principal);
        $this->scorings()->sync($request->scoring);
        $this->certificates()->delete();
        $this->certificates()->createMany($request->certificate);
        $this->upload($request->file);
        return $this->updateScore();
    }
    public function upload(array $files) {
        if (!file_exists('storage')) {
            Artisan::call('storage:link');
        }

        if (!file_exists('storage/principal/' . $this->id)) {
            mkdir('storage/principal/' . $this->id, recursive: true);
        }

        foreach ($files as $file) {
            if (is_null($file['id'])) {
                $name = $file['name'] . '.' . last(explode('.', $file['upload']->getClientOriginalName()));
                $file['upload']->storeAs($this->id, $name, 'principal');

                $this->files()->create([
                    'name' => $name,
                ]);
            } elseif ($file['is_deleted']) {
                $id = $file['id'];
                $original = PrincipalFile::findOrFail($id);
                Storage::disk('principal')->delete("/{$original->principal_id}/$original->name");
                $original->delete();
            } else {
                $id = $file['id'];
                $original = PrincipalFile::findOrFail($id);
                $name = $file['name'] . $original->extension;
                Storage::disk('principal')->move("/{$original->principal_id}/$original->name", "/{$original->principal_id}/$name");
                $original->update(['name' => $name]);
            }
        }
    }
    private function updateScore(): bool{
        return $this->update([
            'score' => $this->scorings->count() / Scoring::whereNull('category')->count() * 10
        ]);
    }
    public function hapus(){
        try {
            $this->scorings()->detach();
            $this->certificates()->delete();
            foreach ($this->files as $file) {
                Storage::disk('principal')->delete("/{$file->principal_id}/$file->name");
                $file->delete();
            }
            return $this->delete();
        } catch (Exception $ex) {
            throw new Exception("Data ini tidak dapat dihapus karena sedang digunakan di data lain!", 422);
        }
    }
    public function sync(){
        $url = config('app.jamsyar_url') . 'Api/add_principal_sbd?from=JSH';
        $response = Http::asJson()->acceptJson()->withToken(Jamsyar::token())
        ->post($url, [
            "nama_principal" => $this->name,
            "alamat_principal" => $this->address,
            "npwp_principal" => $this->npwp_number,
            "email_principal" => $this->email,
            "propinsi_principal" => $this->city->province_id,
            "kota_principal" => $this->city_id,
            "kota_sumber_dana" => $this->city_id,
            "jenis_obligee" => 1,
            "kategori" => 0,
            "nama_penanggung_jawab" => $this->pic_name,
            "jabatan_penanggung_jawab" => $this->pic_position,
            "ktp_penanggung_jawab" => "",
            "npwp_penanggung_jawab" => ""
        ]);
        if($response->successful()){
            $data = $response->json()['data'];
            if($response->json()['status'] == "00"){
                $status = 'Menunggu Approval';
                if($data['kode_unik_principal']) $status = 'Sinkron';
                return $this->update([
                    'jamsyar_id' => $data['kode_principal'],
                    'jamsyar_code' => $data['kode_unik_principal'],
                    'status' => $status
                ]);
            }else{
                throw new Exception($response->json()['keterangan'], 422);
            }
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
    public static function jamsyarSearch(string $keyword){
        $response = Jamsyar::principals([
            "nama_principal"=> $keyword,
            "kode_unik_principal"=> "",
            "limit" => 20,
            "offset" => 0
        ]);
        return $response['data'];
        /*
            $nextOffset = true;
            $offset = 0;
            $dataCount = 0;
            while($nextOffset) {
                $response = Jamsyar::principals([
                    "nama_principal"=> "",
                    "kode_unik_principal"=> "",
                    "limit" => 20,
                    "offset" => $offset
                ]);
                self::upsert(array_filter(array_map(function($principal){
                    \Log::info($principal['nama_principal'].' '.$principal['alamat_principal'].' '.$principal['kode_unik_principal']);
                    if($principal['nama_principal'] && $principal['alamat_principal'] && $principal['kode_unik_principal']){
                        return [
                            'name' => $principal['nama_principal'],
                            'address' => $principal['alamat_principal'],
                            'jamsyar_code' => $principal['kode_unik_principal'],
                            'status' => 'Sinkron'
                        ];
                    }
                },$response['data'])),['jamsyar_code'],['name','address']);
                DB::table('principals')->update(['created_at' => now(),'updated_at' => now()]);

                if(config('app.env') == 'local'){
                    if($dataCount >= 50){
                        $nextOffset = false;
                    }else{
                        $dataCount += 20;
                        $offset += 20;
                    }
                }else if(config('app.env') == 'production'){
                    if($response['total_record'] < 20){
                        $nextOffset = false;
                    }else{
                        $dataCount += 20;
                        $offset += 20;
                    }
                }
            }
        */
    }
    public static function jamyarInsert(array $request){
        return self::updateOrCreate(
            ['name' => $request['name']],[
                'jamsyar_code' => $request['code'],
                'address' => ($request['address'] != "null") ? $request['address'] : null,
                'pic_name' => ($request['picName'] != "null") ? $request['picName'] : null,
                'pic_position' => ($request['picPosition'] != "null") ? $request['picPosition'] : null,
                'status' => 'Sinkron'
            ]
        );
    }
    private static function fetchJamsyarStatus(array $request): object{
        return (object)[
            'jamsyar_code' => $request['kode_unik_principal'],
            'params' => [
                'status' => $request['status'] === '00' ? 'Sinkron' : 'Ditolak',
                'note' => $request['keterangan'] ?? null,
            ]
        ];
    }
    public static function updateJamsyarStatus(array $request): bool{
        $request = self::fetchJamsyarStatus($request);
        $principal = self::where('jamsyar_code',$request->jamsyar_code)->first();
        if(!$principal) throw new Exception("Kode Unik Principal tidak ditemukan", 422);

        return $principal->update($request->params);
    }
}
