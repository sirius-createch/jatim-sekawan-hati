<?php

namespace App\Models;

use Barryvdh\Debugbar\Facades\Debugbar;
use DB;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Branch extends Model
{
    use HasFactory, HasSlug, SoftDeletes;
    protected $fillable = ['name','slug','is_regional','jamsyar_username','jamsyar_password','regional_id'];
    protected $casts = ['is_regional' => 'boolean'];
    protected $appends = ['jamsyar_password_masked'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()->generateSlugsFrom('name')->saveSlugsTo('slug');
    }

    // Accessor

    public function jamsyarPasswordMasked(): Attribute
    {
        return Attribute::make(get: fn() => Str::mask($this->jamsyar_password, "*", 0));
    }

    // Relations

    public function branches(){
        return $this->hasMany(Branch::class,'regional_id');
    }
    public function guarantee_bank(){
        return $this->hasMany(GuaranteeBank::class);
    }
    public function user(){
        return $this->hasOne(User::class);
    }
    public function regional(){
        return $this->belongsTo(Branch::class,'regional_id');
    }
    private static function fetch(object $args): array{
        $params['branch'] = [
            'name' => $args->name,
            'is_regional' => $args->is_regional,
            'jamsyar_username' => $args->jamsyar_username,
            'jamsyar_password' => $args->jamsyar_password,
        ];
        $params['user'] = [
            'name' => $args->login_name,
            'username' => $args->login_username,
            'role' => $args->is_regional ? 'regional' : 'branch',
        ];

        if (!empty($args->login_password)) {
            $params['user']['password'] = Hash::make($args->login_password);
        }

        if($params['branch']['is_regional'] == 0){
            $params['branch']['regional_id'] = $args->regionalId;
        }
        Debugbar::info($params);
        return $params;
    }
    public static function buat(array $params): self{
        $branch = self::create(self::fetch((object)$params)['branch']);
        $branch->user()->create(self::fetch((object)$params)['user']);
        return $branch;
    }
    public function ubah(array $params): bool{
        $this->update(self::fetch((object)$params)['branch']);
        $this->user()->update(self::fetch((object)$params)['user']);
        return true;
    }
    public function hapus(): bool{
        try{
            $this->user()->delete();
            return $this->delete();
        } catch (Exception $ex) {
            throw new Exception("Data ini tidak dapat dihapus karena sedang digunakan di data lain!", 422);
        }
    }
    public function table(){
        return DB::table('branches as b')->join('payables as p','p.regional_id','b.id')->where([
            ['p.regional_id',$this->id],
            ['p.is_paid_off',0]
        ])->select('p.*');
    }
}
