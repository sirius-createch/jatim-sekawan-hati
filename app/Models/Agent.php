<?php

namespace App\Models;

use Database\Seeders\BankSeeder;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use \Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'identity_number',
        'is_active',
        'is_verified',
        'branch_id'
    ];

    /**
     * Interact with the branchesLabel attribute.
     *
     * return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function branchesLabel(): Attribute
    {
        return Attribute::get(fn () => implode(', ',$this->branches()->pluck('name')->toArray()));
    }

    private static function fetch(object $args): object{
        return (object)[
            'agent' => [
                'name' => $args->name,
                'phone' => $args->phone,
                'email' => $args->email,
                'address' => $args->address,
                'identity_number' => $args->identity_number,
                'is_active' => $args->is_active,
                'is_verified' => $args->is_verified,
                // 'branch_id' => $args->branch_id,
            ],
            'branches' => $args->branch_id
        ];
    }

    // public function branch(){
    //     return $this->belongsTo(Branch::class,'branch_id');
    // }

    /**
     * The branches that belong to the Agent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function branches(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class);
    }

    public function bank_accounts(){
        return $this->hasOne(BankAccount::class,'agent_id');
    }
    public function agent_rates(){
        return $this->hasMany(AgentRate::class);
    }

    public static function buat(array $params): self{
        $request = self::fetch((object)$params);

        $agent = self::create($request->agent);
        $agent->branches()->sync($request->branches);
        return $agent;
    }
    public function ubah(array $params): bool{
        $request = self::fetch((object)$params);

        $this->branches()->sync($request->branches);
        return $this->update($request->agent);
    }
    public function hapus(): bool{
        try {
            if ($this->bank_accounts) {
                $this->bank_accounts->each(function($account) {
                    $account->delete();
                });
            }
            return $this->delete();
        } catch (Exception $ex) {
            throw new Exception("Data ini tidak dapat dihapus karena sedang digunakan di data lain!", 422);
        }
    }
}
