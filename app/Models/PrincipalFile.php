<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PrincipalFile extends Model
{
    use HasFactory;

    public $fillable = ['name'];

    protected $appends = ['link', 'extension'];

    // Accessors

    public function link(): Attribute
    {
        return Attribute::make(get: fn () => asset("storage/principal/{$this->principal_id}/$this->name"));
    }

    public function extension(): Attribute
    {
        return Attribute::make(get: fn () => '.' . last(explode('.', $this->name)));
    }

    // Relations

    public function principal(): BelongsTo {
        return $this->belongsTo(Principal::class);
    }
}
