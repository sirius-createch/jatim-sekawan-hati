<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Exception;
use Session;
class Jamsyar
{
    private static function login($username,$password){
        $url = config('app.jamsyar.url') . 'Auth/login_icpr';
        $response = Http::acceptJson()->post($url, [
            'username' => $username,
            'password' => $password,
        ]);
        if($response->successful()){
            $token = $response->json()['token'];
            Session::put('jamsyar',[
                'token' => Crypt::encryptString($token),
                'exp' => $response->json()['exp']
            ]);
            return $token;
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
    public static function token(){
        $username = config('app.jamsyar.username');
        $password = config('app.jamsyar.password');
        if(Session::has('jamsyar')){
            $jamsyar = Session::get('jamsyar');
            if($jamsyar['exp'] > date('Y-m-d H:i:s',strtotime('+ 5 minutes'))){
                return Crypt::decryptString($jamsyar['token']);
            }else{
                return self::login($username,$password);
            }
        }else{
            return self::login($username,$password);
        }
    }
    public static function cities($params = []){
        $url = config('app.jamsyar.url') . 'Api/get_kota';
        if(empty($params)) $params = ["kode_kota" => "","nama_kota" => "","kode_propinsi" => "","limit" => 20,"offset" => 0];

        $response = Http::asJson()->acceptJson()->withToken(self::token())->post($url, $params);
        if($response->successful()){
            return $response->json();
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
    public static function provinces($name = null){
        $url = config('app.jamsyar.url') . 'Api/get_propinsi';
        $response = Http::asJson()->acceptJson()->withToken(self::token())
        ->post($url, [
            'kode_propinsi' => null,
            'nama_propinsi' => $name
        ]);
        if($response->successful()){
            return $response->json()['data'];
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
    public static function principals($params = []){
        $url = config('app.jamsyar.url') . 'Api/get_principal?from=JSH';
        if(empty($params)) $params = ['nama_principal' => '','kode_unik_principal' => '','limit' => 20,'offset' => 0];

        $response = Http::asJson()->acceptJson()->withToken(self::token())->post($url, $params);
        if($response->successful()){
            return $response->json();
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
    public static function obligees($params = []){
        $url = config('app.jamsyar.url') . 'Api/get_obligee?from=JSH';
        if(empty($params)) $params = ['nama_obligee' => '','kode_unik_obligee' => '','limit' => 20,'offset' => 0];

        $response = Http::asJson()->acceptJson()->withToken(self::token())->post($url, $params);
        if($response->successful()){
            return $response->json();
        }else{
            throw new Exception($response->json()['keterangan'], 422);
        }
    }
}
